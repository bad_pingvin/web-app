﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;
using WebApplication1.Models.DataBase;

namespace WebApplication1.Controllers
{
    public class ComentController : Controller
    {
        // GET: Coment
        public ActionResult Partial(Guid id)
        {
            List<Coment> coments = DbProvider.Instance.GetComents(id).ToList();
            return PartialView(coments);
        }
    }
}