﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;
using WebApplication1.Models.DataBase;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        readonly int countPosts = 5;
        public ActionResult Index(int page = 1)
        {
            User user1 = new User
            {
                Id = Guid.NewGuid(),
                Name = "user1",
                HashPassword = "999999",
                State = StateUser.Live
            };

            User user2 = new User
            {
                Id = Guid.NewGuid(),
                Name = "user2",
                HashPassword = "111111",
                State = StateUser.Live
            };

        //    DbProvider.Instance.InsertUsers(new List<User> { user1, user2 });

            Post post1 = new Post
            {
                Id = Guid.NewGuid(),
                Author = user1.Id,
                DateCreate = DateTime.Now,
                Title = "Post1",
                Content = "Text1",
                State = State.Publish
            };

            Post post2 = new Post
            {
                Id = Guid.NewGuid(),
                Author = user1.Id,
                DateCreate = DateTime.Now,
                Title = "Post2",
                Content = "Text2",
                State = State.Publish
            };

            Post post3 = new Post
            {
                Id = Guid.NewGuid(),
                Author = user1.Id,
                DateCreate = DateTime.Now,
                Title = "Post3",
                Content = "Text3",
                State = State.Publish
            };

        //    DbProvider.Instance.InsertPosts(new List<Post> { post1, post2, post3 });

            Coment coment1 = new Coment
            {
                 Id= post1.Id,
                  Author= user1.Id,
                   State=State.Publish,
                    Text="Coment1"
            };

          

            Coment coment3 = new Coment
            {
                Id = post2.Id,
                Author = user1.Id,
                State = State.Publish,
                Text = "Coment1"
            };

          //  DbProvider.Instance.InsertComents(new List<Coment> { coment1, coment3 });

            IEnumerable<Post> posts = DbProvider.Instance.GetPosts(--page, countPosts);         
           
            PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = countPosts, TotalItems = DbProvider.Instance.GetCountPosts(),  };
            IndexViewModel ivm = new IndexViewModel { PageInfo = pageInfo, Posts = posts };
            return View(ivm);
        }

        public ActionResult Post(string Id)
        {
            Post post = DbProvider.Instance.GetPost(new Guid(Id));

            return View(post);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Partial(Guid id)
        {
            List<Coment> coments = DbProvider.Instance.GetComents(id).ToList();
            return PartialView(coments);
        }
    }
}