﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Helpers
{
    public static class PagingHelpers
    {
        public static MvcHtmlString PageLinks(this HtmlHelper html, PageInfo pageInfo, Func<int, string> pageUrl)
        {
            StringBuilder result = new StringBuilder();

            TagBuilder tag = new TagBuilder("a");
           
            if (pageInfo.PageNumber > 1)
            {
                tag.InnerHtml = "<- Prev";
                tag.MergeAttribute("href", pageUrl( pageInfo.PageNumber - 1));
                tag.AddCssClass("btn btn-default");
                result.Append(tag);
            }
            //else
            //{
            //    tag.MergeAttribute("disabled", "disabled");
            //    tag.AddCssClass("noLink");
            //}
          

            tag = new TagBuilder("a");
            tag.MergeAttribute("href", pageUrl(1));
            tag.InnerHtml = "<<";
            tag.AddCssClass("btn btn-default");
            result.Append(tag);

            int star = pageInfo.PageNumber - (int)((pageInfo.PageSize - 1) / 2);
            if(star<1)
                star= 1;
            if (star + pageInfo.PageSize > pageInfo.TotalPages)
                star = pageInfo.TotalPages - pageInfo.PageSize;

            for (int i = star; i< pageInfo.PageSize+ star && i< pageInfo.TotalPages; i++)
            {
                tag = new TagBuilder("a");
                tag.MergeAttribute("href", pageUrl(i));
                tag.InnerHtml = i.ToString();
                // если текущая страница, то выделяем ее,
                // например, добавляя класс
                if (i == pageInfo.PageNumber)
                {
                    tag.AddCssClass("selected");
                    tag.AddCssClass("btn-primary");
                }
                tag.AddCssClass("btn btn-default");
                result.Append(tag.ToString());
            }


            tag = new TagBuilder("a");
            tag.MergeAttribute("href", pageUrl(pageInfo.TotalPages-1));
            tag.InnerHtml = ">>";
            tag.AddCssClass("btn btn-default");
            result.Append(tag);

            tag = new TagBuilder("a");
            if (pageInfo.PageNumber < pageInfo.TotalPages-1)
            {
                tag.InnerHtml = "Next ->";
                tag.MergeAttribute("href", pageUrl(pageInfo.PageNumber + 1));
                tag.AddCssClass("btn btn-default");
                result.Append(tag);
            }

            return MvcHtmlString.Create(result.ToString());
        }

      
       
    }
}