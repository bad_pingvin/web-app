﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Web;
using WebApplication1.Models.DataBase;

namespace WebApplication1.Models
{
    public class DbProvider
    {
        private string PathDateBase
        {
            get
            {
                return Environment.CurrentDirectory + "\\" + System.Configuration.ConfigurationManager.AppSettings["PathName"];
            }
        }

        private SQLiteConnection _connection;

        private DbProvider()
        { }
        private static readonly DbProvider _instance = new DbProvider();

        public static DbProvider Instance
        {
            get
            {
                return _instance;
            }
        }


        public void OpenConnection()
        {

            if (!File.Exists(PathDateBase))
                CreateTables();

            if (_connection == null)
            {
                _connection = new SQLiteConnection(string.Format("Data Source={0};Version=3;", PathDateBase));
                if(_connection.State!= System.Data.ConnectionState.Open)
                _connection.Open();
            }
           
        }


        public void CloseConnection()
        {
            if (_connection != null)
                _connection.Dispose();

            _connection = null;
        }


        public void CreateTables()
        {
            try
            {
                SQLiteConnection.CreateFile(PathDateBase);
                _connection = new SQLiteConnection(string.Format("Data Source={0};Version=3;", PathDateBase));
                _connection.Open();
                _connection.Execute(@"
                                        CREATE TABLE IF NOT EXISTS [User] (
                                        [Id] uniqueidentifier NOT NULL PRIMARY KEY,
                                        [Name] NVARCHAR(128) NOT NULL,
                                        [State] integer NOT NULL,
                                        [HashPassword] NVARCHAR(128) NOT NULL);

                                        CREATE TABLE IF NOT EXISTS [Post] (
                                        [Id] uniqueidentifier NOT NULL PRIMARY KEY,
                                        [Author] uniqueidentifier NOT NULL,
                                        [Title] NVARCHAR(128) NOT NULL,
                                        [Content] NVARCHAR(1000) NOT NULL,
                                        [State] integer NOT NULL,
                                        [DateCreate] TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                                        CONSTRAINT FK_AuthorPost FOREIGN KEY (Author)     
                                        REFERENCES User (Id)     
                                        ON DELETE CASCADE    
                                        ON UPDATE CASCADE);

CREATE TABLE IF NOT EXISTS [Coment]  ( 
[Id] uniqueidentifier NOT NULL PRIMARY KEY, 
[Author] uniqueidentifier NOT NULL, 
[Text] NVARCHAR(1000) NOT NULL, 
[State] integer NOT NULL,  
CONSTRAINT FK_AuthorComent FOREIGN KEY (Author) 
REFERENCES User (Id) 
ON DELETE CASCADE 
ON UPDATE CASCADE);

");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Post> GetPosts(int page, int countPosts)
        {
            try
            {
                OpenConnection();
                var posts = _connection.Query<Post>(@"SELECT * from [Post] 
                                                                WHERE State='0'
                                                                ORDER BY Id
																LIMIT @fetchCount 
                                                                OFFSET @skipCount;",
                                                                new { skipCount = page * countPosts, fetchCount = countPosts });
                return posts;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public int GetCountPosts()
        {
            try
            {
                OpenConnection();

                object reader = _connection.ExecuteScalar("select count(*) from [Post]");
                return Convert.ToInt32(reader);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public Post GetPost(Guid id)
        {
            try
            {
                OpenConnection();

                var item = _connection.Query<Post>("select count(*) from [Post] where id=@id", new { id=id});
                return item.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }


        public IEnumerable<Coment> GetComents(Guid id)
        {
            try
            {
                OpenConnection();

                var items = _connection.Query<Coment>("select * from [Coment]", new { id = id });
                return items;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public void InsertUsers(IEnumerable<User> users)
        {
            try
            {
                OpenConnection();
                foreach (var user in users)
                    _connection.Execute("insert into User(Id,Name,HashPassword,State) values(@Id,@Name,@HashPassword,@State)", user);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public void InsertPosts(IEnumerable<Post> posts)
        {
            try
            {
                OpenConnection();
                foreach (var post in posts)
                    _connection.Execute("insert into Post(Id,DateCreate,Title,Content,Author,State) values(@Id,@DateCreate,@Title,@Content,@Author,@State)", post);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public void InsertComents(IEnumerable<Coment> coments)
        {
            try
            {
                OpenConnection();
                foreach (var post in coments)
                    _connection.Execute("insert into Coment(Id,Text,Author,State) values(@Id,@Text,@Author,@State)", post);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }
    }
}