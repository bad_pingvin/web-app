﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models.DataBase
{
    public class Post
    {
        public Guid Id { get; set; }
        public DateTime DateCreate { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }

        public Guid Author { get; set; }

        public State State { get; set; }
    }

    public class User
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string HashPassword { get; set; }
        public StateUser State { get; set; }
    }

    public enum StateUser
    {
        Live,
        ReadOnly,
        Blocked,
        Remove
    }

    public enum State
    {
        Publish,
        Hiden,
        Remove
    }

    public class Coment
    {
        public Guid Id { get; set; }
        public Guid Author { get; set; }
        public string Text { get; set; }
        public State State { get; set; }
    }

}